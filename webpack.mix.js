const mix = require('laravel-mix');

/*
 

|---------------------------------------------------------------------

-----
 | Mix Asset Management
 

|---------------------------------------------------------------------

-----
 |
 | Mix provides a clean, fluent API for defining some Webpack build 

steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

mix.js('resources/js/app.js', 'public/js')
    //.sass('resources/sass/app.scss', 'public/css');

.styles([
    'node_modules/@coreui/coreui/dist/css/coreui.css',
    'node_modules/@coreui/icons/css/free.min.css',
    'node_modules/@coreui/icons/css/brand.min.css',
    'node_modules/@coreui/icons/css/flag.min.css'
], 'public/css/app.css')

.scripts([
    'node_modules/@coreui/coreui/dist/js/coreui.bundle.min.js',
    'node_modules/@coreui/coreui/dist/js/main.js',
    'node_modules/@coreui/utils/coreui-utils.js',
    'node_modules/@coreui/icons/js/svgxuse.min.js'
], 'public/js/app2.js');

mix.copy('node_modules/@coreui/icons/fonts', 'public/fonts');
mix.copy('node_modules/@coreui/icons/svg/flag', 'public/svg/flag');
mix.copy('node_modules/@coreui/icons/sprites/', 'public/icons/sprites');
mix.copy('node_modules/@coreui/icons/svg/free/', 'public/icons/svg/free');