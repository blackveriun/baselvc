<!doctype html>
    <html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
        <head>
            <meta charset="utf-8">
            <meta http-equiv="X-UA-Compatible" content="IE=edge">
            <meta name="viewport" content="width=device-width, initial-scale=1.0, shrink-to-fit=no">
            <meta name="csrf-token" content="{{ csrf_token() }}">
            <title>{{ config('app.name', 'AIPP') }}</title>
            <link rel="dns-prefetch" href="//fonts.gstatic.com">
            <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet">
            <link rel="stylesheet" href="{{ asset('css/app.css')}}">
            <script>
                window.dataLayer = window.dataLayer || [];
                function gtag(){dataLayer.push(arguments);}
                gtag('js', new Date());
                gtag('config', 'UA-118965717-1');
              </script>
            <script>
                window.dataLayer = window.dataLayer || [];
                function gtag() {
                    dataLayer.push(arguments);
                    }
                    gtag('js', new Date());
                    // Shared ID
                    gtag('config', 'UA-118965717-3');
                    // Bootstrap ID
                    gtag('config', 'UA-118965717-5');
            </script>
        </head>
    <body class="c-app c-dark-theme">
        <div id="app">
            @include('cms.sideleft')
            @include('cms.sideright')
            <div class="c-wrapper">
                @include('cms.navbar')
                <div class="c-body" style="padding-top: 8%;">
                    @include('cms.body')
                    @include('cms.footer')
                </div>
            </div>  
        </div>
        <script src="{{asset('js/app.js')}}" defer></script>
        <script src="{{asset('js/app2.js')}}" defer></script>
        <script src="{{asset('/js/main.js')}}"></script>
        <script>
            new coreui.AsyncLoad(document.getElementById('ui-view'));
            var tooltipEl = document.getElementById('header-tooltip');
            var tootltip = new coreui.Tooltip(tooltipEl);
        </script>
    </body>
</html>