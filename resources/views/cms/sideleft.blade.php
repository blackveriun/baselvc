
<div class="c-sidebar c-sidebar-dark c-sidebar-fixed c-sidebar-lg-show" id="sidebar">
    <div class="c-sidebar-brand d-lg-down-none">
      <a href="{{ url('/') }}">
        <svg class="c-sidebar-brand-full" width="118" height="46" alt="CoreUI Logo">
          <a href="{{ url('/') }}">
          <use xlink:href="{{asset('assets/brand/coreui.svg#full')}}"></use>
        </svg>
      
        <svg class="c-sidebar-brand-minimized" width="46" height="46" alt="CoreUI Logo">
          <use xlink:href="{{asset('assets/brand/coreui.svg#signet')}}"></use>
        </svg>
      </a>
    </div>
    
    <ul class="c-sidebar-nav" data-drodpown-accordion="true">
      <li class="c-sidebar-nav-item"><a class="c-sidebar-nav-link" href="{{url('/')}}">
        <svg class="c-sidebar-nav-icon">
              
          <use xlink:href="{{asset('icons/sprites/free.svg#cil-speedometer')}}"></use>
          
        </svg> Escritorio
        <!-- <span class="badge badge-danger">Alpha 0.1.1</span> -->
        </a></li>
    
        <li class="c-sidebar-nav-title">Modulos</li>
        

        <li class="c-sidebar-nav-item"><a class="c-sidebar-nav-link" href="{{url('/home')}}">
          <svg class="c-sidebar-nav-icon">
            <use xlink:href="{{asset('icons/sprites/free.svg#cil-bank
            ')}}"></use>
          </svg> Inicio</a></li>
    
          <li class="c-sidebar-nav-item">
            <a class="c-sidebar-nav-link" href="{{url('/vue')}}">
            <svg class="c-sidebar-nav-icon">
              <use xlink:href="{{asset('icons/sprites/free.svg#cil-av-timer')}}"></use>
            </svg>Modulo vue</a>
          </li>

        </ul>
        
<button class="c-sidebar-minimizer c-class-toggler" type="button" data-target="_parent" data-class="c-sidebar-minimized"></button>
</div>
