<header class="c-header c-header-light c-header-fixed c-header-with-subheader">

<ul class="c-header-nav d-md-down-none">
  <li class="c-header-nav-item px-3"><a class="c-header-nav-link" href="#"></a></li>
</ul>

    <button class="c-header-toggler c-class-toggler d-lg-none mfe-auto" type="button" data-target="#sidebar" data-class="c-sidebar-show">
    <svg class="c-icon c-icon-lg">
    <use xlink:href="vendors/@coreui/icons/svg/free.svg#cil-menu"></use>
    </svg>
    </button><a class="c-header-brand d-lg-none" href="#">
    <svg class="c-sidebar-brand-full" width="118" height="146" alt="CoreUI Logo">
    <!-- <use xlink:href="assets/brand/coreui-pro.svg#full"></use> -->
    <use xlink:href="{{asset('assets/brand/coreui.svg#full')}}"></use>
    </svg></a>
    <button class="c-header-toggler c-class-toggler mfs-3 d-md-down-none" type="button" data-target="#sidebar" data-class="c-sidebar-lg-show" responsive="true">
    <svg class="c-icon c-icon-lg">
    
    <use xlink:href="{{asset('icons/sprites/free.svg#cil-menu')}}"></use>
    </svg>
    </button>
    <ul class="c-header-nav d-md-down-none">
    <!-- <li class="c-header-nav-item px-3"><a class="c-header-nav-link" href="#">Escritorio</a></li> -->
    <!-- <li class="c-header-nav-item px-3"><a class="c-header-nav-link" href="#">test</a></li> -->
    <li class="c-header-nav-item px-3"><a class="c-header-nav-link" href="#">Usuarios</a></li>
    <!-- <li class="c-header-nav-item px-3"><a class="c-header-nav-link" href="#">Configuraciones</a></li> -->
    </ul>
    <ul class="c-header-nav mfs-auto">
    <li class="c-header-nav-item px-3 c-d-legacy-none">
    <button class="c-class-toggler c-header-nav-btn" type="button" id="header-tooltip" data-target="body" data-class="c-dark-theme" data-toggle="c-tooltip" data-placement="bottom" title="Toggle Light/Dark Mode">
    <svg class="c-icon c-d-dark-none">
    
    <use xlink:href="{{asset('icons/sprites/free.svg#cil-menu')}}"></use>
    </svg>
    <svg class="c-icon c-d-default-none">
      
      <use xlink:href="{{asset('icons/sprites/free.svg#cil-sun')}}"></use>
    </svg>
    </button>
    </li>

    </ul>
    <ul class="c-header-nav">
    <li class="c-header-nav-item dropdown d-md-down-none mx-2"><a class="c-header-nav-link" data-toggle="dropdown" href="#" role="button" aria-haspopup="true" aria-expanded="false">
    <svg class="c-icon">
    
    <use xlink:href="{{asset('icons/sprites/free.svg#cil-bell')}}"></use>
    </svg><span class="badge badge-pill badge-danger">5</span></a>
    <div class="dropdown-menu dropdown-menu-right dropdown-menu-lg pt-0">
    <div class="dropdown-header bg-light"><strong>Tienes 5 notificaciones</strong></div><a class="dropdown-item" href="#">
    <svg class="c-icon mfe-2 text-success">
    
    <use xlink:href="{{asset('icons/sprites/free.svg#cil-user-follow')}}"></use>
    </svg> Nuevos Usuarios Registrados &nbsp; <span class="badge badge-danger">1</span></a><a class="dropdown-item" href="#">
    <svg class="c-icon mfe-2 text-danger">
    
    <use xlink:href="{{asset('icons/sprites/free.svg#cil-user-unfollow')}}"></use>
    </svg> Usuarios Eliminados &nbsp; <span class="badge badge-danger">1</span></a><a class="dropdown-item" href="#">
    <svg class="c-icon mfe-2 text-info">
    
    <use xlink:href="{{asset('icons/sprites/free.svg#cil-chart')}}"></use>
    </svg> Informe de Venta &nbsp; <span class="badge badge-danger">1</span></a><a class="dropdown-item" href="#">
    <svg class="c-icon mfe-2 text-success">
    
    <use xlink:href="{{asset('icons/sprites/free.svg#cil-basket')}}"></use>
    </svg> Nuevo Cliente &nbsp; <span class="badge badge-danger">1</span></a><a class="dropdown-item" href="#">
    <svg class="c-icon mfe-2 text-warning">
    
    <use xlink:href="{{asset('icons/sprites/free.svg#cil-speedometer')}}"></use>
    </svg> Servidor sobrecargado &nbsp; <span class="badge badge-danger">1</span>
    </a>
    
    </li>
    <li class="c-header-nav-item dropdown d-md-down-none mx-2"><a class="c-header-nav-link" data-toggle="dropdown" href="#" role="button" aria-haspopup="true" aria-expanded="false">
    <svg class="c-icon">
    
    <use xlink:href="{{asset('icons/sprites/free.svg#cil-list-rich')}}"></use>
    </svg><span class="badge badge-pill badge-warning">15</span></a>
    <div class="dropdown-menu dropdown-menu-right dropdown-menu-lg pt-0">
    <div class="dropdown-header bg-light"><strong>5 Avances en curso</strong></div><a class="dropdown-item d-block" href="#">
    <div class="small mb-1">Upgrade NPM &amp; Bower<span class="float-right"><strong>0%</strong></span></div><span class="progress progress-xs">
    <div class="progress-bar bg-info" role="progressbar" style="width: 0%" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100"></div>
    </span>
    </a><a class="dropdown-item d-block" href="#">
    <div class="small mb-1">ReactJS Version<span class="float-right"><strong>25%</strong></span></div><span class="progress progress-xs">
    <div class="progress-bar bg-danger" role="progressbar" style="width: 25%" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100"></div>
    </span>
    </a><a class="dropdown-item d-block" href="#">
    <div class="small mb-1">VueJS Version<span class="float-right"><strong>50%</strong></span></div><span class="progress progress-xs">
    <div class="progress-bar bg-warning" role="progressbar" style="width: 50%" aria-valuenow="50" aria-valuemin="0" aria-valuemax="100"></div>
    </span>
    </a><a class="dropdown-item d-block" href="#">
    <div class="small mb-1">Add new layouts<span class="float-right"><strong>75%</strong></span></div><span class="progress progress-xs">
    <div class="progress-bar bg-info" role="progressbar" style="width: 75%" aria-valuenow="75" aria-valuemin="0" aria-valuemax="100"></div>
    </span>
    </a><a class="dropdown-item d-block" href="#">
    <div class="small mb-1">Angular 8 Version<span class="float-right"><strong>100%</strong></span></div><span class="progress progress-xs">
    <div class="progress-bar bg-success" role="progressbar" style="width: 100%" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100"></div>
    </span>
    </a><a class="dropdown-item text-center border-top" href="#"><strong>Ver todos los Avances</strong></a>
    </div>
    </li>
    <li class="c-header-nav-item dropdown d-md-down-none mx-2"><a class="c-header-nav-link" data-toggle="dropdown" href="#" role="button" aria-haspopup="true" aria-expanded="false">
    <svg class="c-icon">
    
    <use xlink:href="{{asset('icons/sprites/free.svg#cil-envelope-open')}}"></use>
    
    </svg><span class="badge badge-pill badge-info">7</span></a>
    <div class="dropdown-menu dropdown-menu-right dropdown-menu-lg pt-0">
    <div class="dropdown-header bg-light"><strong>Tienes 4 mensajes</strong></div><a class="dropdown-item" href="#">
    <div class="message">
    <div class="py-3 mfe-3 float-left">
    <div class="c-avatar"><img class="c-avatar-img" src="{{asset('assets/img/avatars/7.jpg')}}" alt="user@email.com"><span class="c-avatar-status bg-success"></span></div>
    </div>
    <div><small class="text-muted">John Doe</small><small class="text-muted float-right mt-1">Justo ahora</small></div>
    <div class="text-truncate font-weight-bold"><span class="text-danger">!</span> Mensaje importante</div>
    <div class="small text-muted text-truncate">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt...</div>
    </div>
    </a><a class="dropdown-item" href="#">
    <div class="message">
    <div class="py-3 mfe-3 float-left">
    <div class="c-avatar"><img class="c-avatar-img" src="{{asset('assets/img/avatars/6.jpg')}}" alt="user@email.com"><span class="c-avatar-status bg-warning"></span></div>
    </div>
    <div><small class="text-muted">John Doe</small><small class="text-muted float-right mt-1">hace minutos 5</small></div>
    <div class="text-truncate font-weight-bold">Lorem ipsum dolor sit amet</div>
    <div class="small text-muted text-truncate">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt...</div>
    </div>
    </a><a class="dropdown-item" href="#">
    <div class="message">
    <div class="py-3 mfe-3 float-left">
    <div class="c-avatar"><img class="c-avatar-img" src="{{asset('assets/img/avatars/5.jpg')}}" alt="user@email.com"><span class="c-avatar-status bg-danger"></span></div>
    </div>
    <div><small class="text-muted">John Doe</small><small class="text-muted float-right mt-1">1:52 PM</small></div>
    <div class="text-truncate font-weight-bold">Lorem ipsum dolor sit amet</div>
    <div class="small text-muted text-truncate">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt...</div>
    </div>
    </a><a class="dropdown-item" href="#">
    <div class="message">
    <div class="py-3 mfe-3 float-left">
    <div class="c-avatar"><img class="c-avatar-img" src="{{asset('assets/img/avatars/4.jpg')}}" alt="user@email.com"><span class="c-avatar-status bg-info"></span></div>
    </div>
    <div><small class="text-muted">John Doe</small><small class="text-muted float-right mt-1">4:03 PM</small></div>
    <div class="text-truncate font-weight-bold">Lorem ipsum dolor sit amet</div>
    <div class="small text-muted text-truncate">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt...</div>
    </div>
    </a><a class="dropdown-item text-center border-top" href="#"><strong>Ver todos los mensajes</strong></a>
    </div>
    </li>
    
    <li class="c-header-nav-item dropdown"><a class="c-header-nav-link" data-toggle="dropdown" href="#" role="button" aria-haspopup="true" aria-expanded="false">
    <div class="c-avatar"><img class="c-avatar-img" src="{{asset('assets/img/avatars/blackveriun.jpg')}}" alt="blackveriun@gmail.com"></div> &nbsp; Blackveriun
    </a>
    <div class="dropdown-menu dropdown-menu-right pt-0">
    <div class="dropdown-header bg-light py-2"><strong>Cuenta</strong></div><a class="dropdown-item" href="#">
    <svg class="c-icon mfe-2">
      <use xlink:href="{{asset('icons/sprites/free.svg#cil-bell')}}"></use>
    </svg> Actualizaciones<span class="badge badge-info mfs-auto">42</span></a><a class="dropdown-item" href="#">
    <svg class="c-icon mfe-2">
    
    <use xlink:href="{{asset('icons/sprites/free.svg#cil-envelope-open')}}"></use>
    </svg> Mensajes<span class="badge badge-success mfs-auto">42</span></a><a class="dropdown-item" href="#">
    <svg class="c-icon mfe-2">
    
    <use xlink:href="{{asset('icons/sprites/free.svg#cil-task')}}"></use>
    </svg> Tareas<span class="badge badge-danger mfs-auto">42</span></a><a class="dropdown-item" href="#">
    <svg class="c-icon mfe-2">
    
    <use xlink:href="{{asset('icons/sprites/free.svg#cil-comment-square')}}"></use>
    </svg> Comentarios<span class="badge badge-warning mfs-auto">42</span></a>
    <div class="dropdown-header bg-light py-2"><strong>Configuraciones</strong></div><a class="dropdown-item" href="#">
    <svg class="c-icon mfe-2">
    
    <use xlink:href="{{asset('icons/sprites/free.svg#cil-user')}}"></use>
    </svg> Perfil</a><a class="dropdown-item" href="#">
    <svg class="c-icon mfe-2">
     
     <use xlink:href="{{asset('icons/sprites/free.svg#cil-settings')}}"></use>
    </svg> Configuraciones</a><a class="dropdown-item" href="#">
    <svg class="c-icon mfe-2">
    
    <use xlink:href="{{asset('icons/sprites/free.svg#cil-credit-card')}}"></use>
    </svg> Pagos<span class="badge badge-secondary mfs-auto">42</span></a><a class="dropdown-item" href="#">
    <svg class="c-icon mfe-2">
    
    <use xlink:href="{{asset('icons/sprites/free.svg#cil-file')}}"></use>
    </svg> Proyectos<span class="badge badge-primary mfs-auto">42</span></a>
    <div class="dropdown-divider"></div><a class="dropdown-item" href="#">
    <svg class="c-icon mfe-2">
    
    <use xlink:href="{{asset('icons/sprites/free.svg#cil-lock-locked')}}"></use>
    </svg> Bloquear cuenta</a>

<a class="dropdown-item" href="">
  <svg class="c-icon mfe-2">
    <use xlink:href="{{asset('icons/sprites/free.svg#cil-account-logout')}}"></use>
  </svg>Cerrar Session</a>

 <form id="logout-form" action="" method="POST" style="display: none;">

</form>
    </div>
    </li>

    <!-- <li class="c-header-nav-item">
    otro item
    </li> -->


    <button class="c-header-toggler c-class-toggler mfe-md-3" type="button" data-target="#aside" data-class="c-sidebar-show">
    <svg class="c-icon c-icon-lg">
    
      <use xlink:href="{{asset('icons/sprites/free.svg#cil-menu')}}"></use>
    </svg>
    </button>
    </ul>
    
    <div class="c-subheader justify-content-between px-3">
    
    <ol class="breadcrumb border-0 m-0 px-0 px-md-3">
    <li class="breadcrumb-item">Inicio</li>
    <li class="breadcrumb-item"><a href="#">Admin</a></li>
    <li class="breadcrumb-item active">Escritorio</li>
    
    </ol>
    <!-- <div class="c-header-nav d-md-down-none mfe-2"><a class="c-header-nav-link" href="#">
    <svg class="c-icon">
    
    <use xlink:href="{{asset('icons/sprites/free.svg#cil-speedometer')}}"></use>
    </svg></a><a class="c-header-nav-link" href="#">
    <svg class="c-icon">
    
    <use xlink:href="{{asset('icons/sprites/free.svg#cil-graph')}}"></use>
    </svg> &nbsp;Escritorio</a><a class="c-header-nav-link" href="#">
    <svg class="c-icon">
      <use xlink:href="{{asset('icons/sprites/free.svg#cil-settings')}}"></use>
    </svg> &nbsp;Configuraciones</a></div>
    </div> -->
  
    </header>